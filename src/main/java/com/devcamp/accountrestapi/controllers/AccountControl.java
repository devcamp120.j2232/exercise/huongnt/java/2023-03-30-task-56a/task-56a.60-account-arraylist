package com.devcamp.accountrestapi.controllers;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.service.annotation.PutExchange;

import com.devcamp.accountrestapi.Account;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AccountControl {
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
        //khởi tạo 3 đối tượng account có tham số
        Account account1 = new Account("001", "Lan", 1000000);
        Account account2 = new Account("002", "Ngoc", 2000000);
        Account account3 = new Account("003", "Hung", 3000000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());

        //tạo danh sách lớp Account
        ArrayList<Account> accountsList = new ArrayList<>();
        //thêm đối tượng
        accountsList.add(account1);
        accountsList.add(account2);
        accountsList.add(account3);
        return accountsList;

    }
}
