package com.devcamp.accountrestapi;

public class Account {
    private String id;
    private String name;
    private int balance = 0;
    
    //khởi tạo phương thức
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    //getter
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    //add amount to balance
    public int credit(int amount){
        this.balance += amount;
        return this.balance;
    }
    
    public int debit(int amount){
        if(amount <= balance){
            this.balance -= amount;
        }
        else {
            System.out.println("Amount excceeded balanace");
        }
        return this.balance;
    }
    
    public int tranferTo(Account another, int amount){
        if(amount <= balance){
            another.balance += amount;
            this.balance -= amount;
        }
        else {
            System.out.println("Amount excceeded balanace");
        }
        return this.balance;
    }

    //in ra console
    @Override
    public String toString() {
        return "Account [id=" + id + ", name=" + name + ", balance=" + balance + "]";
    }

    
}
